
public class MathApp {

	public static void main(String[] args) {

		ziffernSumme(623);

		int[] zahlen = new int[] { 2, 3, 4, 5, 6 };
		int[] zahlen1 = new int[] { 2, 2, 3, 4, 2, 5, 2, 6 };

		mittelwert(zahlen);

		min(zahlen);

		max(zahlen);

		zurueck(zahlen1, 2);
		zurueck(zahlen1, 5);

	}

	public static void ziffernSumme(int zahl) {
		
		
		int ziffer;
		int summe = 0;

		while (zahl > 0) { // solange zah �ber 0 ist
			ziffer = zahl % 10; // wird durch %10 letzte ziffer im int ziffer gespeichert

			summe = summe + ziffer; // und zur summe gez�hlt

			zahl = zahl / 10; // dann wird durch 10 dividiert, um letzte zimmer zu entfernen und mit der
								// n�chsten zu rechnen
								// durch int geht kommstelle verloren -> kein double nehmen!
		}
		System.out.println(summe);
	}

	public static double mittelwert(int[] zahlenArray) {

		double summe = 0;

		for (int zahl : zahlenArray) {

			summe = summe + zahl;
		}
		summe = summe / zahlenArray.length;

		System.out.println(summe);
		return summe;
	}

	public static int min(int[] zahlenArray) {

		int min = zahlenArray[0];

		for (int index = 0; index < zahlenArray.length; index++) {

			if (min > zahlenArray[index]) {
				min = zahlenArray[index];
			}
		}
		System.out.println(min);
		return min;
	}

	public static int max(int[] zahlenArray) {
		int max = zahlenArray[0];

		for (int index = 0; index < zahlenArray.length; index++) {
			if (max < zahlenArray[index]) {
				max = zahlenArray[index];
			}
		}
		System.out.println(max);
		return max;
	}

	public static int zurueck(int[] zahlenArray, int zahl) {
		int summe = 0;

		for (int index : zahlenArray) {

			if (index == zahl) {
				summe++;
			}
		}

		System.out.println(summe);
		return summe;
	}
}
