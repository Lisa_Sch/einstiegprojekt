import java.util.ArrayList;

import org.campus.personen.Person;

public class PersonenApp
{

	public static void main(String[] args)
	{

		Person person1 = new Person("Uschi", "Huber");

		System.out.println(person1);

		Person person2 = new Person("Otto", "Bauer");

		Person person3 = new Person("Julia", "Bauer");

		person1.setVater(person2);
		person1.setMutter(person3);

		System.out.println(person1.toString());
		System.out.println(person1.getVater());

		Person kind1 = new Person("Susi", "Bauer");
		Person kind2 = new Person("Paul", "Bauer");

		ArrayList<Person> kinder = new ArrayList<Person>();

		kinder.add(kind1);
		kinder.add(kind2);

		person1.setKinder(kinder);

		person1.setKinder(kinder);

		System.out.println(person1);
		;

	}

}
