package org.campus.personen;

import java.util.ArrayList;

public class Person
{

	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;


	 private ArrayList<Person> kind = new ArrayList<Person>();

	private static int anzahlPersonen; // wenn ich variable als static, gibt es sie nur einmal in ganzer Klasse.
										// alle Objekte teilen sich variable, nicht durch Objekt ver�nderbar.
										// private = nicht von au�en aufgreifbar.

	public Person(String vorname, String nachname)
	{

		this.vorname = vorname; // this. verweist auf Variable in DIESER Klasse
								// Brauche ich wenn ich von au�en Parameter mitgebe, der den gleichen
								// Variablennamen hat.
								// Wert von au�en wird auf Parameter innerhalb der Klasse �bertragen.

		this.nachname = nachname;

		anzahlPersonen++; // bei jedem Erstellen von neuem Objekt, erh�ht sich anzahl um eins. gibt es nur
							// einmal f�r ganze Klasse, gelten f�r jedes Objekt.
	}

	public void setVater(Person vater) // getVariablennamen() / setVariablennamen()
	{
		this.vater = vater; // immer dann wenn ich Klasse schreibe und ich sp�ter von au�en Parameter
							// zuf�hren will -> Methode mit set/get

	}

	public void setMutter(Person mutter) // set ist Methode, die auf Datentyp und Wert (Person) von au�en wartet ->
											// mache ich erst sp�ter
											// set mache ich, wenn ich will, dass sp�ter von au�en noch variablen
											// zugegeben werden.
	{
		this.mutter = mutter;
	}

	public Person getVater() // get ist Methode, die Variable von au�en abfragen k�nnen. Wenn ich nicht von
								// au�en ausgeben lassen will, dann keine get-Methode.
	{
		return vater;
	}

	public void setKinder(ArrayList<Person> kind)
	{
		this.kind = kind;
		
	}

	public String toString()
	{
		String string ="";
		
		for (Person kind : kind)
		{
			string = string + kind.toString();
		}
		
		return String.format("(%s %s) (%s %s) (%s) %d", vorname, nachname, mutter, vater, kind, anzahlPersonen); // %s=String;
		// %2f=2 Kommastellen;
		// %d=ganze Zahl
		// .format formatiert
	}

}
