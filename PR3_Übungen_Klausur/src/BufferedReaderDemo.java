import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.BufferedReader;

public class BufferedReaderDemo {

	public static void main(String[] args) {
		
		try (
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				// System.in (liest Eingabe von Console ein)
				// System.out (schreibt in Console)
				)
		{
		
			
			
			String line;
			while((line = br.readLine()) != null) {
				
				if(line.equals("STOP")) {
					return;
				}
				else {
					System.out.println("Eingabe: " + line);
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
