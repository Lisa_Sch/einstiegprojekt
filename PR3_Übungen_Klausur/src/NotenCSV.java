import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;

public class NotenCSV {

	public static void main(String[] args) {
		
		try(
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				PrintWriter pw = new PrintWriter("noten.csv");
				)
		{
			String line;
			
			while((line = br.readLine()) != null) {
				if (line.equals("STOP"))
					return;
				pw.println(line);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
