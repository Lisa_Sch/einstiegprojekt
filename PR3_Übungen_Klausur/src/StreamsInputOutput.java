import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class StreamsInputOutput {

	public static void main(String[] args) {
		
		try (
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				PrintWriter pw = new PrintWriter(new OutputStreamWriter (new FileOutputStream("test.txt")));
				)
		{
			System.out.println("Ihre Eingabe:");
			String line;
			while((line = br.readLine()) != null) {
				if(line.equals("STOP"))
					return;
				
			pw.println(line);	
			}
			pw.flush();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
