import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ObjectOutputStream {

	public static void main(String[] args) {
		
		String s = "Hallo, dies ist eine Datei.";
		try(
				
				OutputStream os = new FileOutputStream("output.txt");
				InputStream is = new BufferedInputStream(new FileInputStream("output.txt"));
				OutputStream console = System.out; 
				)
		{
			for (char c : s.toCharArray()) {
				os.write(c);
			}
			
			os.flush();
			
			int b;
			while((b=is.read()) != 0) {
				
				
				console.write(b);
			}
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		}

	}

}
