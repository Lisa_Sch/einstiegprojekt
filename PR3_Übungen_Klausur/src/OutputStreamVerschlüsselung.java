import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamVerschlüsselung extends FilterOutputStream {

	public OutputStreamVerschlüsselung(OutputStream arg0) {
		super(arg0);
	}

	public void write(int b, int faktor) throws IOException {
		
		super.write(b*faktor);
	}
	
	



}
