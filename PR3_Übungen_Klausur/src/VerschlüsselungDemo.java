import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class VerschlüsselungDemo {

	public static void main(String[] args) {

try (
		BufferedInputStream bi = new BufferedInputStream(System.in);
		OutputStreamVerschlüsselung os = new OutputStreamVerschlüsselung(new FileOutputStream("verschlüsselung.txt"));
		OutputStreamVerschlüsselung os2 = new OutputStreamVerschlüsselung(System.out);
		
		){
	
	int b;
	while((b = bi.read()) != -1) {
		os.write(b, 2);
		os2.write(b, 2);
		}
	
	os.flush();
	os2.flush();
	}
	
 catch (FileNotFoundException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (IOException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}

	}

}
