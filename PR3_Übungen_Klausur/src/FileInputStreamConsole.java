import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileInputStreamConsole {

	public static void main(String[] args) {
		
		try(
				InputStream fi = new BufferedInputStream(System.in);
				)
		{
			int b;
			while((b=fi.read()) !=0) {
				
				char c = Character.toChars(b)[0];
				
				if (c == 'x' || c == 'X')
					return;
				System.out.println(c);
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}

}
