package org.campus02.tree;



public class Bin�rbaum {

		private TreeNode root;
	
	public void add(String value) {

		if (root == null) {
			root = new TreeNode(value);
			return;
		}
		
		root.add(value); 
	}

	public void preorder() {

		if(root != null)
		   root.preorder();
	}

	public boolean searchPreorder(String pattern) {

		if(root != null)
		   return root.searchPreorder(pattern);
		
		return false;
	}	
	
	public boolean inOrderSearch(String search) {
		if(root != null) {
			return root.inOrderSearch(search);
		}
		return false;
	}
	
	public void inOrder() {
		if(root != null) {
			 root.inOrder();
		}
		
	}
			
	
		
	}
			
