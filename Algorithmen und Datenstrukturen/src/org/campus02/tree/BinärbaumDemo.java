package org.campus02.tree;



public class BinärbaumDemo {

	public static void main(String[] args) {
		
		Binärbaum tree = new Binärbaum();
		
		tree.add("D");
		tree.add("F");
		tree.add("C");
		tree.add("E");
		tree.add("A");		
		tree.add("B");
		
		System.out.println("PreOrder:");
		
		tree.searchPreorder("B");
		
		System.out.println();
		
		System.out.println("InOrder:");
		
		tree.inOrderSearch("B");
		
		System.out.println();
		
		tree.inOrder();

	}
}
