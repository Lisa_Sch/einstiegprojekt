package org.campus02.tree;



public class TreeNode {

	private String value;
	private TreeNode next;
	private TreeNode left;
	private TreeNode right;
	
	
	public TreeNode (String value) {
		this.value=value;
	}
	
	
	
	public String getValue() {
	return value;	
	}

	public void setNext(TreeNode next)
	{
		this.next = next;
	}

	public void setLeft(TreeNode left)
	{
		this.left = left;
	}

	public void setRight(TreeNode right)
	{
		this.right = right;
	}

	public TreeNode getNext()
	{
		return next;
	}

	public TreeNode getLeft()
	{
		return left;
	}

	public TreeNode getRight()
	{
		return right;
	}

	


	public void add(String newValue)
	{
	
		
		if(newValue.compareTo(value) < 0) {							// neuer Wert ist kleiner als Knoten
			
			if(left==null)
				left = new TreeNode(newValue);
			
			else
				left.add(newValue);
			
		} if(newValue.compareTo(value) > 0) {						// neuer Wert ist gr��er als Knoten 
			
			if(right==null)
				right = new TreeNode(newValue);
			
			else 
				right.add(newValue); 								// ruft add() f�r rechten Knoten auf, weil links und rechts schon besetzt
		}
		
	}
	
	public void preorder() {

		System.out.println("Node: " + value);
		
		if (left != null)
			left.preorder();
		
		if (right != null)
			right.preorder();
	}

	
	public boolean searchPreorder(String pattern) {

	
		System.out.println("Node: " + value);
		
		if (value.equals(pattern)) {
			System.out.println("Found: " + value);
			return true;
		}
		
		if (left != null) {
			boolean success = left.searchPreorder(pattern); 				// sonst gehe ich rechten Ast auch noch durch 
			
			if (success)
				return true;
		}
		
		if (right != null) {
			boolean success = right.searchPreorder(pattern);
		
			if (success)
				return true;
		}
		
		return false;
	}



	public boolean inOrderSearch(String search)
	{
		
		if (left != null) {
			boolean success = left.inOrderSearch(search);
			System.out.println("Node: " + value);
			
			if (success) {
				return true;
			}
		}
		
		if(right != null) {
			boolean success = right.inOrderSearch(search);
			
			if(success) {
				return true;
			}
		}
		
		if(value.equals(search)) {
			System.out.println("Found: " + value +"");
			return true;
		}
		
		return false;
	}
	
	public void inOrder () {
		if(left != null)
			left.inOrder();
		
		System.out.println("node: "+ value);
		
		if(right != null)
			right.inOrder();
	}
	
	
	
}
