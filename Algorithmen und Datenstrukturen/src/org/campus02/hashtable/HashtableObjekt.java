package org.campus02.hashtable;

public class HashtableObjekt
{

	private Hashbucket[] array;
	
	public HashtableObjekt()
	{
		this.array = new Hashbucket[7];
	}



	public void add (String s) {
		
		int hash = Math.abs(s.hashCode());								// damit keine negativen Hashwerte 
		int index = hash % array.length;
		
		Hashbucket bucket = array[index];
		
		if (bucket == null) {
			bucket = new Hashbucket();
			array[index] = bucket;
		}
		
		System.out.println("Hashcode " + hash);

		System.out.println("Add " + s + " at index: " + index);
		
		bucket.add(s);
		
	}



	public void remove(String string)
	{
		int hash = Math.abs(string.hashCode());
		int index = hash % array.length;
		
		Hashbucket hb = array[index];
		
		if (hb==null) {
			return;
		} 
		
		System.out.println("Remove "+string+ " at index "+index);
		
		hb.remove(string);
		
	}
	
	public void print () {
		for (Hashbucket hashbucket : array)
		{
			System.out.println(hashbucket);
		}
	}
	
	
	
	

}
