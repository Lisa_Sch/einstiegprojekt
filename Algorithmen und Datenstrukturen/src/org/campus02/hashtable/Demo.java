package org.campus02.hashtable;

public class Demo
{

	public static void main(String[] args)
	{
		HashtableObjekt ht = new HashtableObjekt();
		ht.add("Hund");
		ht.add("Katze");
		ht.add("Maus");
		ht.add("Tiger");
		ht.add("Elefant");
		ht.add("Schildkröte");
		
		System.out.println();
		
		ht.remove("Katze");
		ht.remove("Hund");
		
	}

}
