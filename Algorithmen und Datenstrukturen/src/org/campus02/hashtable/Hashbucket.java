package org.campus02.hashtable;

import java.util.ArrayList;

public class Hashbucket
{
	
	private ArrayList<String>list;
	
	public Hashbucket () {
		list = new ArrayList<>();
	}

	public void add(String value)
	{
		 list.add(value);
		 System.out.println("Anzahl der Objekte des Hashbuckets " + list.size());
		
	}
	
	
	
	public void remove (String s) {
		list.remove(s);						// arraylist pr�ft mit equals, welches das richtige Element ist 
		System.out.println("Anzahl der Objekte des Hashbuckets " + list.size());
	}

	@Override
	public String toString()
	{
		return "Hashbucket [list=" + list + "]";
	}
	
	
}
