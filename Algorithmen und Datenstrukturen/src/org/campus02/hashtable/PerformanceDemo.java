package org.campus02.hashtable;

import java.util.ArrayList;



public class PerformanceDemo
{

	public static void main(String[] args)
	{
		ArrayList<Integer> list = new ArrayList<>();
		
		long startTime = System.nanoTime(); 										// gibt aktuelle Systemzeit in nano-Sekunden
		
	
		for(int i=0; i< 1000000; i++) {
			list.add(i);
		}

		long endTime = System.nanoTime();
		
		long diff = (endTime-startTime) / 1000000;
		
		System.out.println(diff+ " ms");
		
		
		
		
	}

}
