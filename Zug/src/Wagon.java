
public class Wagon
{

	private String ladung;
	private Wagon nachbar; // Nachbarwagon vom Objekt Wagon (Datentyp) erstellen

	public Wagon(String inhalt) // Konstruktor, der Inhalt des Wagons verlangt
	{
		ladung = inhalt;
	}

	public void einhaengen(Wagon neuerNachbar)
	{
		nachbar = neuerNachbar;
	}

	public String toString() // beim Auschreiben ruft er Methode auf und schreibt sinnvolle Strings
								
	{
		return ladung;
	}

	public Wagon gibMirNachbar() //brauch ich, damit jeder Wagon wei�, wer sein Nachbar ist 
	{
		return nachbar;
	}
}
