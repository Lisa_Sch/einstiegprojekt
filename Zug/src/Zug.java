
public class Zug
{

	private Wagon erster;
	private Wagon letzter;
	private int zuglaenge = 0;

	public Zug() // Konstruktor ohne Inhalt wird automatisch erzeugt
	{

	}

	public void neuerWagon(String inhalt)
	{
		Wagon neuerWagon = new Wagon(inhalt); // 1. Schritt ist immer neuen Wagon zu erstellen

		if (erster == null) // pr�fen, ob an erster Stelle schon Wagon steht; null hei�t, noch kein Wagon
		{
			erster = neuerWagon;
			letzter = neuerWagon;
		} else // wenn es schon mind. 1 Wagon gibt, dann neuen Wagon an letztem anh�ngen
		{
			letzter.einhaengen(neuerWagon);
			letzter = neuerWagon;
		}

		zuglaenge++;
	}

	public int length()
	{
		return zuglaenge;
	}

	public String wagonInhaltAnStelle(int stelle)
	{

		if (stelle > zuglaenge - 1 || stelle < 0)
		{
			return "Stelle nicht vorhanden";
		}

		Wagon temp = erster;
		for (int springen = 0; springen < stelle; springen++)
		{
			temp = temp.gibMirNachbar();
		}

		return temp.toString();
	}

	public String toString()
	{
		if (erster == null)
		{
			return "leer";
		}

		else
		{
			String ergebnis = "Mein Zug hat Laenge " + zuglaenge + "  und diese Inhalte: ";

			Wagon temp = erster;

			while (temp != null) // ich h�pfe solange von Wagon zu Wagon, bis letzter Wagon nicht mehr existiert
			{
				ergebnis = ergebnis + " " + temp.toString(); // schreibt ergebnis-String mit toString-Methode von Klasse Wagon (weil temp=Wagon)
				temp = temp.gibMirNachbar(); // dadurch wechselt temp auf Nachbar
			}

			return ergebnis;
		}
	}


}


