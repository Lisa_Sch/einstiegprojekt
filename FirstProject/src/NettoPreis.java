
public class NettoPreis
{

	public static void main(String[] args)
	{

		berechneNetto(200, 2);
		berechneNetto(200, 1);
		berechneNetto(200, 3);

		BruttoNettoKategorie.bruttoPreis(200, 2);

		
	
	}

	// aus brutto netto machen

	public static double berechneNetto(double bruttoPreis, int steuerkategorie)
	{
		double nettoPreis;

		switch (steuerkategorie)
		{
		case 1:
			nettoPreis = bruttoPreis / 1.2;
			break;

		case 2:
			nettoPreis = bruttoPreis / 1.12;
			break;

		case 3:
			nettoPreis = bruttoPreis / 1.1;
			break;

		default:
			nettoPreis = bruttoPreis;
		}

		System.out.println("Das ist der Nettopreis: " + nettoPreis);
		return nettoPreis;
	}

}
