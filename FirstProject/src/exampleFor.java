
public class exampleFor
{

	public static void main(String[] args)
	{
		

		
		System.out.println(sumUp(13));
		
		System.out.println(sumUpHalf(10));
		
		System.out.println(sumUpBruch(10));
	}

	public static void printNumbers(int counter)
	{
		for (int index = 5; index < counter; index = index + 2)
		{
			System.out.println(index);
		}

	}

	public static int sumUp(int max)
	{

		int result = 0;

		for (int index = 1; index <= max; index++)
		{
			result = result + index;
		}

		return result;
		

	}

	public static double sumUpHalf (int maximum) {
		
		double ergebnis = 0;
		
		for (double index =1; index <= maximum; index++) {
			
			ergebnis = ergebnis + (index/2);
		}
		
		return ergebnis;
		
	}

	public static double sumUpBruch (int bruch)
	{
		double ergebnis=0;
		
		for (double index=2; index <=bruch; index++)
		{
			ergebnis = ergebnis + (1/index);
		}
		
		return ergebnis; 
	}

}
