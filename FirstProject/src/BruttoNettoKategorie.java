
public class BruttoNettoKategorie
{
	public static void main(String args[])
	{

		double brutto = bruttoPreis(100, 1);

		System.out.println(brutto);
		

		double netto = nettoPreis(112, 2);

		System.out.println(netto);
		
		brutto = bruttoPreis (200,3);
		System.out.println("Das ist der Bruttopreis: " + brutto);
		
	}

	public static double bruttoPreis(double nettoPreis, int kategorie)
	{
		double ergebnisBrutto;

		switch (kategorie)
		{

		case 1:
			ergebnisBrutto = nettoPreis * 1.2;
			break;

		case 2:
			ergebnisBrutto = nettoPreis * 1.12;
			break;

		case 3:
			ergebnisBrutto = nettoPreis * 1.1;
			break;

		default:
			ergebnisBrutto = nettoPreis;
			break;
		}

		
		System.out.println("Das ist der Bruttopreis: " + ergebnisBrutto);
		return ergebnisBrutto;

	}

	public static double nettoPreis(double bruttoPreis, int kategorie) // Methode, um Nettopreis zu berechnen

	{
		double ergebnisNetto;

		switch (kategorie)
		{
		case 1:
			ergebnisNetto = bruttoPreis / 1.2;
			break;

		case 2:
			ergebnisNetto = bruttoPreis / 1.12;
			break;

		case 3:
			ergebnisNetto = bruttoPreis / 1.1;
			break;

		default:
			ergebnisNetto = bruttoPreis;
		}

		return ergebnisNetto;
	}

}
