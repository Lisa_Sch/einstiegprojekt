
public class ForEach
{

	public static void main(String[] args)
	{
		int[] zahlen = new int[] { 2, 5, 3, 1, 7, 22, 33, 42 };

		sumUp(zahlen);

	}

	public static int sumUp(int[] feld)
	{

		int result = 0;

		for (int wert : feld)
		{
			result = result + wert;

		}

		System.out.println(result);
		return result;

	}
}
