
public class ArrayExample
{

	public static void main(String[] args)
	{
		int[] feld1 = new int[] { 1, 2, 3, 4 };


		printArray(feld1);
		
		ArrayMax.findeMax(feld1);
		
		
		printArray(feld1); // Ausgabe vor Tausch
		swap(feld1,3,1); // Methode swap wird aufgerufen
		printArray(feld1); // Ausgabe nach Tausch 
		
		
	}

	public static void printArray(int[] array)

	{

		for (int index = 0; index < array.length; index++)
		{
			System.out.print(array[index] + " ");
		}
		System.out.println();
	}


	public static void swap(int [] array, int a, int b)
{
	int temp = array [a]; // Zwischenspeichern des Wertes auf Feld a int temp
	
	array [a] = array [b]; // Wert des Feldes b auf Feld a schreiben
	
	array [b] = temp; // gemerkten Wert auf Feld a schreiben
	
}

}
