
public class ArrayMax
{

	public static void main(String[] args)
	{

		int[] a = new int[]	{ 2, 3, 5, 1, 3, 10 }; // array deklarieren, mit dem ich arbeiten will
		int max = findeMax(a); // Ergebnis der Methode findeMax, die mit dem array arbeitet, wird in int max gespeichert 
		System.out.println(max);
		
		
	}

	public static int findeMax(int[] feld) // Methode mit dem Parameter array []
	{
		int result = feld[0]; // das erste Max ist der Wert an der ersten Stelle

		for (int index = 0; index < feld.length; index++)
		{

			if (result < feld[index])
			{
				result = feld[index];
			}
		}
		System.out.println(result);
		return result;

	}
}
