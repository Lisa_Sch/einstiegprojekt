
public class WhileExample
{

	public static void main(String[] args)
	{
		double einlage = 2000;
		double kontostand = einlage;
		double zinssatz = 1.025;
		double zielbetrag = 3000;
		int jahre = 0;

		while (kontostand < zielbetrag)
		{
			jahre++;
			kontostand = kontostand * zinssatz;

			System.out.printf("%d: %.2f \n", jahre, kontostand); // auf zwei Kommastellen und Abstaz
		}
		System.out.println(jahre + "jahre");
	}

}
