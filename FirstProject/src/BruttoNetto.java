
public class BruttoNetto
{
	public static void main(String args[])
	{

		double nettoPreis = 12.5;

		double steuersatz = 0.2;

		double brutto = nettoPreis + (nettoPreis * steuersatz);

		System.out.printf("netto:       %.2f \n", nettoPreis); // /n macht Absatz
		System.out.printf("steuersatz:  %.2f \n", steuersatz);
		System.out.printf("Bruttopreis: %.2f \n", brutto); // %.2f rundet auf 2 Kommastellen

		int a = 4; // = Zuweisung

		if (a == 2) // if braucht immer einen Boolean; == Vergleich
		{
			System.out.println("a ist gleich 2");
		} else if (a == 3)
		{
			System.out.println("a ist gleich 3");
		} else
		{
			System.out.println("a ist nicht gleich 2");
		}
	}
}
