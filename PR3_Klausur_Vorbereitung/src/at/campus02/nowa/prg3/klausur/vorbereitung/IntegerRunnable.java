package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class IntegerRunnable implements Runnable {

	private static int sum; 							// static, weil er sich die Zahlen merkt 
	private static int counter;
	private Socket socket;

	public IntegerRunnable(Socket socket) {
		super();
		this.socket = socket;
	
	}

	@Override
	public void run() {

		

			try (	BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					PrintWriter pw = new PrintWriter(socket.getOutputStream())
					)
				
			
			{
				String line;

				while ((line = br.readLine()) != null) {

					try {
						synchronized (IntegerRunnable.class) {			// sobald etwas berechnet und zur�ckgesendet wird -> synchronized
							
							int i = Integer.parseInt(line);
							sum += i;
							counter++;
							pw.println("Aktueller Duchschnitt aller empfangener Integer: " + (double)sum / counter);
						}

						
						
					} catch (NumberFormatException e) {
						pw.println("Ung�ltige Eingabe");
					}
					pw.flush();
					

				}
			} catch (IOException e) {

				e.printStackTrace();
			}
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

