package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.ServerSocket;
import java.net.Socket;

public class ServerDemo {

	public static void main(String[] args) {

		try (ServerSocket server = new ServerSocket(4000))

		{
			while (true) {

				Socket socket = server.accept();
				System.out.println("Accepted: " + socket);

				IntegerRunnable ir = new IntegerRunnable(socket);

				Thread t = new Thread(ir);

				t.start();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
