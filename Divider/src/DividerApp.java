import java.util.ArrayList;

import java.util.Collections;

public class DividerApp {

	public static void main(String[] args) {

		ArrayList<Integer> teilerListe = new ArrayList<Integer>();

		for (int teiler = 1; teiler < 42; teiler++) {
			int ergebnis = 42 % teiler;

			if (ergebnis == 0) {
				teilerListe.add(teiler);
			}

		}

		System.out.println(Collections.max(teilerListe));
		
		greatestDivider(16);
		greatestDivider(71);
		greatestDivider(123);
		greatestDivider(1024);
	}
	
	public static void greatestDivider(int ganzzahl)
	{
		ArrayList<Integer> teilerListe = new ArrayList<Integer>();

		for (int teiler = 1; teiler < ganzzahl; teiler++) {
			int ergebnis = ganzzahl % teiler;

			if (ergebnis == 0) {
				teilerListe.add(teiler);
			}

		}

		System.out.println(Collections.max(teilerListe));
	}

}
