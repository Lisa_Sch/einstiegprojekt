import java.util.Scanner;

public class Consolentest
{

	public static void main(String[] args)
	{
		System.out.println("Eingabe: ");
		
		Scanner vonConsole = new Scanner(System.in);// neues Objekt von Klasse Scanner; zeigt auf Speicherplatz 
		
		int vonConsoleGelesen = vonConsole.nextInt();
		
		System.out.println(vonConsoleGelesen);
		
		System.out.println("Ihren Namen bitte: ");
		String name = vonConsole.next();
		System.out.printf("Hallo %s", name);
		
		
		vonConsole.close();
	}

}
