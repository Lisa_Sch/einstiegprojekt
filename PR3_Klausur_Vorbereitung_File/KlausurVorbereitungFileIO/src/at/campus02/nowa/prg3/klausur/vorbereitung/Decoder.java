package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UTFDataFormatException;

public class Decoder {

	public static void main(String[] args) {
		
		// streams einlesen: bufferedinputStream
		// streams schreiben: fileoutputstream
		
		try 
			(ByteInverterInputStream bi = new ByteInverterInputStream(new FileInputStream("secret.dat"));
			FileOutputStream fo = new FileOutputStream("nachricht.txt"))
			
			
		{	
			int b;
			while((b=bi.read()) != -1) {
				fo.write(b);
			}
			
			fo.flush();
			
			
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e1) {
		
			e1.printStackTrace();
		}
		
		

	}

}
