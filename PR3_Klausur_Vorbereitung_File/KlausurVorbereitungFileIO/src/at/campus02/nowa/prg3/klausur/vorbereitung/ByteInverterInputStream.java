package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteInverterInputStream extends FilterInputStream {

	protected ByteInverterInputStream(InputStream arg0) {
		super(arg0);
		
	}

	@Override
	public int read() throws IOException {
		int b = super.read(); 					// read() liefert n�chstes byte zur�ck oder -1, wenn Stream zu Ende
		if(b == -1) {
			return -1;							// wenn Stream zu Ende, -1 zur�ck
		}
		int i =Integer.reverse(b << 24) & 0xff;
		return i;
	}


	
	

}
