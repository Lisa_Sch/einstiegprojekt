package org.campus02.filesystem;


public class Datei
{
	private String name;
	private int gr��e;
	
	public Datei (String name,int gr��e)
	{
		this.name=name;
		this.gr��e=gr��e;
	}
	public int getGr��e ()
	{return gr��e;}
	
	public String getName ()
	{
		return name;
	}
	
	public String toString ()
	{
		return String.format("%s, %d", name, gr��e);
	}
}
