package org.campus.uno;

import java.util.ArrayList;

public class Spieler {
	private String name;

	private ArrayList<Karte> handkarten = new ArrayList<Karte>();

	public Spieler(String name) {
		this.name = name;
	}

	public void aufnehmen(Karte karte) {
		handkarten.add(karte);

	}

	public Karte passendeKarte(Karte vergleichendeKarte) // Ich vergleiche die Karte am Ablegestapel mit meinen in
															// meiner Liste
	{
		for (Karte karte : handkarten) {
			if (karte.match(vergleichendeKarte)) {
				handkarten.remove(karte);
				
				if (handkarten.size() ==1)
				{
					System.out.println("  UNO");
				}
				
				return karte;
			}
		}

		return null;
	}
	
	public int anzahlHandkarten()
	{
		return handkarten.size();
	}
	

	

	public String toString() {
		return name + " " + handkarten;
	}
}
