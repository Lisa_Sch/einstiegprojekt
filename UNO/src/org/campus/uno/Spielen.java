package org.campus.uno;

import org.campus.uno.Spieler;

public class Spielen
{

	public static void main(String[] args)
	{
//		Karte karte1 = new Karte(Farbe.blau, 3);
//		Karte karte2 = new Karte(Farbe.gelb, 3);
//		Karte karte3 = new Karte(Farbe.gelb, 4);
//
//		System.out.println(karte1.toString());
//
//		System.out.println(karte1.match(karte2));
//	
//		System.out.println(karte1.compare(karte2));
//		System.out.println(karte1.compare(karte3));
//		System.out.println(karte2.compare(karte3));
//		
		UnoSpiel meinSpiel = new UnoSpiel(); // neues Objekt von UnoSpiel erstellen: macht das, was im Konstruktor steht
		
		Spieler sp1 = new Spieler("Otto"); // new = reserviert Speicherplatz 
		Spieler sp2 = new Spieler("Annabel");
		Spieler sp3 = new Spieler("Franz");

		meinSpiel.mitspielen(sp1);
		meinSpiel.mitspielen(sp2);
		meinSpiel.mitspielen(sp3);
		
		meinSpiel.austeilen();
		
//		System.out.println(sp1.passendeKarte(new Karte(Farbe.blau,2)));
//
//		Karte abgehobeneKarte = meinSpiel.abheben(); // Ich merke mir abgehobene Karte in einer Variablen
//		
//		System.out.println(abgehobeneKarte); //mehrere Karten abheben 
//		System.out.println(meinSpiel.abheben());
		
	
		int zaehler=1;
		
		while (meinSpiel.spielzug())
		{
			System.out.println("Z�ge: " + zaehler);
			zaehler++;
		}
		
	}

}
