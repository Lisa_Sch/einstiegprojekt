package org.campus.uno;

public class Karte
{

	private Farbe farbe;
	private int zahl;

	public Karte(Farbe kartenfarbe, int kartenzahl) // Konstruktor = die Methode, die aufgerufern wird, wenn neues Objekt mit new erstellt wird 
	{ 
		farbe = kartenfarbe;
		zahl = kartenzahl;
		
	}

	
	public boolean match(Karte ablegendeKarte)
	{
		boolean result;
	
			if (farbe == ablegendeKarte.farbe || zahl == ablegendeKarte.zahl)
			{
				result = true;
			} else
			{
				result = false;
			}


		return result;
	}

	public String toString()
	{
		return "Spielkarte: " + farbe + " " + zahl;
	}
	
	public boolean compare(Karte zweiteKarte)
	{
		if (farbe == zweiteKarte.farbe ) // wenn Farben gleich sind, Zahlen vergleichen 
		{
			boolean result = zahl > zweiteKarte.zahl; 
			return result; 
		}
		else 
		{
			return farbe.ordinal() < zweiteKarte.farbe.ordinal(); // ordinal liefert Zahlenwert mit Stelle im enum, wenns zutrifft dann true 
		}
	}
}
