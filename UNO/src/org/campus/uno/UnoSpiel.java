package org.campus.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{

	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();

	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();

	public UnoSpiel()
	{
		for (int index = 0; index < 2; index++) // Ich will von jeder Karte 2 St�ck haben
		{
			for (int zahlenwert = 0; zahlenwert < 10; zahlenwert++) // erstellt von jeder Farbe Karten von 0-9
			{
				kartenStapel.add(new Karte(Farbe.rot, zahlenwert)); // Ich muss neues Objekt (new Karte) nicht neuer
																	// Variable zuordnen,
				kartenStapel.add(new Karte(Farbe.blau, zahlenwert)); // kann das auch einer Methode zuordnen (liste
																		// kartenstapel und add = new Karte)
				kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));
				kartenStapel.add(new Karte(Farbe.greun, zahlenwert));
			}
		}

		Collections.shuffle(kartenStapel); // Karten mischen

	}

	public Karte abheben()
	{
		if (kartenStapel.size()==0)
		{
			Karte obersteKarteAmAbhebestapel = ablageStapel.remove(0);
			
			Collections.shuffle(ablageStapel);
			
			kartenStapel.addAll(ablageStapel);
			
			ablageStapel.clear();
			ablageStapel.add(obersteKarteAmAbhebestapel);
		}
		
		return kartenStapel.remove(0);
		
	}

	public void mitspielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
	}

	public void austeilen() // jeden Mitspieler besuchen in Liste und er nimmt eine Karte bis jeder 7 Karten
							// hat
	{
		for (int zaehler = 0; zaehler < 7; zaehler++)
		{
			for (Spieler sp : mitspieler)
			{
				Karte karte = abheben();
				sp.aufnehmen(karte);
			}
		}

		Karte temp = abheben(); // zum Schluss eine Karte vom Kartenstapel abheben und auf den ablegestapel
								// legen
		ablageStapel.add(temp);
	}

	public void ablegen(Karte ablegeKarte)
	{
		ablageStapel.add(0, ablegeKarte); // wenn meine Karte passt, lege ich meine Karte auf Stapel an oberster Stelle
											// ab
	}

	public boolean spielzug()
	{
		Spieler aktuellerSpieler = mitspieler.remove(0); // erster Spieler ist dran - ich nehme ihn aus Liste raus, mach
															// damit Spielzug
		System.out.printf("am Zug ist: " + aktuellerSpieler);

		Karte gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0)); // ich vergleiche Karten vom Spieler
																					// mit oberster Karte am
																					// Ablagestapel

		if (gefundeneKarte != null)
		{
			ablegen(gefundeneKarte);
			System.out.println(" Karte ablegen:" + gefundeneKarte);
		} else
		{
			Karte abgehobeneKarte = abheben();

			if (abgehobeneKarte.match(ablageStapel.get(0)))
			{
				ablegen(abgehobeneKarte);
				System.out.println(" Karte passt und ablegen: " + abgehobeneKarte);
			} else
			{
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
				System.out.println(" Karte in Handkarten aufnehmen");
			}
		}

		if (aktuellerSpieler.anzahlHandkarten() == 0)
		{
			System.out.println("Gewonnen hat: " + aktuellerSpieler);
		 return false;
		}
	
		
		mitspieler.add(aktuellerSpieler); // am Ende des Spielzuges f�ge ich am Ende der Spielerliste ein
		
		
		return true;
	}

}
