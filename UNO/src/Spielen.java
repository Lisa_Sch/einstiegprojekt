import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class Spielen
{

	public static void main(String[] args)
	{
		Karte karte1 = new Karte(Farbe.blau, 3);
		Karte karte2 = new Karte(Farbe.gelb, 3);
		Karte karte3 = new Karte(Farbe.gelb, 4);

		System.out.println(karte1.toString());

		System.out.println(karte1.match(karte2));
		System.out.println(karte1.match(karte3));

	}

}
