import java.util.ArrayList;

import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KartenArray
{

	public static void main(String[] args)
	{
		Karte [] meineKarten = new Karte [] {new Karte(Farbe.rot,1), new Karte(Farbe.blau, 1), new Karte(Farbe.gelb, 10)};
		
		System.out.println(meineKarten[1]);
		
		for (Karte karte:meineKarten)
		{
			System.out.println(karte);
		}

		// ArrayList mit Datentyp Karte> Name der Liste = neue ArrayList mit Datentyp Karte
		ArrayList<Karte>meineListe= new ArrayList<Karte>();
		
		meineListe.add(new Karte(Farbe.rot,2)); // eine Karte hinten dran in Liste einf�gen
		meineListe.add(new Karte(Farbe.blau,3));
		meineListe.add(new Karte(Farbe.gelb,2));
		
		System.out.println(meineListe.size()); // Wie viele Elemente da drin
		
		System.out.println(meineListe.get(0)); // Wert an Position 0 der ArrayList ausgeben -> mit get-Methode 
		
		for (Karte karte:meineListe)
		{
			System.out.println(karte);
		}
		
		meineListe.add(0, new Karte(Farbe.gelb, 2)); // f�gt an Stelle 0 eine andere Karte ein
		
		System.out.println();
		
		for (Karte karte:meineListe)
		{
			System.out.println(karte);
		}
		System.out.println();
		
		System.out.println(meineListe.size());
		System.out.println(meineListe.remove(0)); // Karte an Position 0 entfernen 
		System.out.println(meineListe.size());
		
		System.out.println(meineListe); // ich erspare mir toString, bei Array brauche ich toString
	}

}
