package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek
{

	private ArrayList<Book> bibliothek = new ArrayList<Book>();

	public void addBook(Book b)
	{

		bibliothek.add(b);
		
		

	}

	public int countPages()
	{
		int summe = 0;

		for (Book book : bibliothek)
		{
			summe = summe + book.getSeiten();
		}

		return summe;
	}

	public double avaragePages()
	{
		double summe = 0;

		for (Book book : bibliothek)
		{
			summe = summe + book.getSeiten();
		}

		summe = summe / bibliothek.size();
		return summe;

	}

	public ArrayList<Book> booksByAuthor(String autor)
	{
		ArrayList<Book> booksByAuthor = new ArrayList<Book>();

		if (autor == null)
		{
			return booksByAuthor;
		}

		else
		{

			for (Book book : bibliothek)
			{
				if (autor == book.getAutor())
				{
					booksByAuthor.add(book);
				}
			}
		}
		return booksByAuthor;
	}

	public ArrayList<Book> findBook(String search)
	{
		ArrayList<Book> findBooks = new ArrayList<Book>();

		if (search == null)
			return findBooks;
		
		else {
		
		for (Book book : bibliothek)
		{
			if (book.match(search))
			{
				findBooks.add(book);
			}
		}

		return findBooks;
	} }

}
