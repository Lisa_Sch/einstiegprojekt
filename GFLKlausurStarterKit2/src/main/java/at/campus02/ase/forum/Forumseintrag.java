package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag {
	
	
	String titel;
	String text;
	ArrayList<Forumseintrag> antworten = new ArrayList<>();

	public Forumseintrag(String titel, String text) {
		this.titel = titel;
		this.text = text; 
	}

	public Forumseintrag antworten(String titel, String text) {
		
		
		Forumseintrag eintrag = new Forumseintrag(titel, text);
		
		antworten.add(eintrag);
		
		return eintrag;
	}


	public ArrayList<Forumseintrag> getAntworten() {
       

		return antworten;
	}

	public int anzahlDerEintraege() {
		
	int summe = 0;
	
	for (Forumseintrag eintrag : antworten)
	{
		summe = summe + eintrag.antworten.size() +1;
	}

		return summe;
	}
	
	public String toString()

	{
		String antwort="";
		
		for (Forumseintrag forumseintrag : antworten)
		{
			antwort=antwort + forumseintrag.toString(); // toString wandelt Objekt forumseintrag in String um
		}
		
		if (!antwort.isEmpty())
		{ antwort = "[" + antwort +"]"; }
		
		return String.format("(%s,%s)%s", titel, text, antwort);
	}
}
