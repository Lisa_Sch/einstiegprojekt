import at.campus02.ase.books.Bibliothek;
import at.campus02.ase.books.Book;

public class BookApp
{

	public static void main(String[] args)
	{
		Bibliothek b1 = new Bibliothek();
		
		Book book1 = new Book("author1", "1", 200);
		Book book2 = new Book("author2", "2", 100);
		Book book3 = new Book("author3", "3", 100);
		
		b1.addBook(book1);
		b1.addBook(book2);
		b1.addBook(book3);
		
	System.out.println(b1.countPages());		
	
		

	}

}
