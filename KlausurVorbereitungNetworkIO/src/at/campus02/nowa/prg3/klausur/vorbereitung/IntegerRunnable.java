package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class IntegerRunnable implements Runnable {

	private Socket socket;
	private Durchschnitt d;

	public IntegerRunnable(Socket socket, Durchschnitt d) {
		this.socket = socket;
		this.d = d;
	}

	@Override
	public void run() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter pw = new PrintWriter(socket.getOutputStream());) {

			String line;
			while ((line = br.readLine()) != null) {
			try {
				int i = Integer.parseInt(line);
				d.getSumme(i);

				pw.println(("Der aktuelle Durchschnitt: " + d.getDurchschnitt()));
				pw.flush();
			} catch (NumberFormatException e) {
				System.out.println("Ung�ltige Eingabe");
			}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
