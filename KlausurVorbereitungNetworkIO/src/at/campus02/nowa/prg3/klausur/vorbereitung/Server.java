package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) {
		
		try {
			ServerSocket so = new ServerSocket(4000);
			Durchschnitt d = new Durchschnitt();
			
			while(true) {
			Socket server = so.accept();
			System.out.println("Accepted " + server);
			
			Thread t = new Thread(new IntegerRunnable(server, d));
			t.start();
			
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
