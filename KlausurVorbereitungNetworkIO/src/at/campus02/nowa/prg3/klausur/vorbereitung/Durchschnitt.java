package at.campus02.nowa.prg3.klausur.vorbereitung;

public class Durchschnitt {

	private int summe;
	private int z�hler;
	
	public synchronized void getSumme(int zahl) {
		summe = summe +zahl;
		z�hler++;
	}
	
	public synchronized double getDurchschnitt() {
		return (float)summe/z�hler;
	}
	
	
}
