
public class LoopApp {

	public static void main(String[] args) {
	
		System.out.println(bruchSumme(100));
		
		System.out.println(sumReihe(9000));

	}
	
	
	public static double bruchSumme(int nenner) {
		
		double summe = 0;
		
		for (double index =2; index <= nenner; index++)
		{
			summe = summe + 1/index;
		}
		
		return summe;
	}

	public static int sumReihe(int max)
	{
		int summe = 0;
		
		for (int index=3; index<= max; index = index+3)
		{
			summe = summe + index;
		}
		
		return summe;
	}
	
}
