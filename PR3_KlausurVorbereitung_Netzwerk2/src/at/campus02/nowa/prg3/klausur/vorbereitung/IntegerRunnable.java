package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class IntegerRunnable implements Runnable {


	private Socket socket;
	private Statistik st;

	public IntegerRunnable(Socket socket, Statistik st) {
		super();
		this.socket = socket;
		 this.st = st;
	
	}

	@Override
	public void run() {

		

			try (	BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					PrintWriter pw = new PrintWriter(socket.getOutputStream())
					)
				
			
			{
				String line;

				while ((line = br.readLine()) != null) {

					try {
						
							int i = Integer.parseInt(line);
							st.add(i);

							pw.println("Aktueller Duchschnitt aller empfangener Integer: " + st.calculate());
						

						
						
					} catch (NumberFormatException e) {
						pw.println("Ung�ltige Eingabe");
					}
					
					pw.flush();
					

				}
			} catch (IOException e) {

				e.printStackTrace();
			}
			try {
				socket.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

