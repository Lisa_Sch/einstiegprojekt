package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main(String[] args) {
		try (
			Socket client = new Socket(InetAddress.getLocalHost(), 4000);									// Verbindung zum Server
			BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));			// liest Nachrichten von Server
			PrintWriter pw = new PrintWriter(client.getOutputStream());										// schickt Nachrichten an Server
			BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));						// liest aus Konsole
		) {
			String line;
			while ((line = brc.readLine()) != null) {
				pw.println(line);							// schickt Consoleneingabe an Server
				pw.flush();
				String answer = br.readLine();				// liest Serverantwort 
				System.out.println(answer);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
