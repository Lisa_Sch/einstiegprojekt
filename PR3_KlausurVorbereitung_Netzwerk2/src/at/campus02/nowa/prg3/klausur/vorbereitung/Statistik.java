package at.campus02.nowa.prg3.klausur.vorbereitung;

public class Statistik
{
	private static int counter;
	private static int summe;
	
	public synchronized void add(int i) {			// alle Clients haben darauf Zugriff --> synchronized 
		summe += i;
		counter++;
	}
	
	public synchronized double calculate() {
		return (float)summe/counter;
	}
}
