
import java.util.LinkedList;
import java.util.ListIterator;

import org.campus02.student.Student;

public class DemoStudent
{

	public static void main(String[] args)
	{

		LinkedList<Student> studenten = new LinkedList<>();			// LinkedList und ArrayList enth�lt Objekte auch �fters. Wenn ich m�chte, dass jedes Objekt nur einmal vorkommt
																	// --> HashSet (Ist aber auch nur eine Liste, ungeordnet)

		studenten.addFirst(new Student("Max"));
		studenten.addFirst(new Student("Peter"));
		studenten.addFirst(new Student("John"));
		studenten.addFirst(new Student("Phil"));

		ListIterator<Student> i = studenten.listIterator();

		i.next(); 											// Phil
		i.next(); 											// John

		i.add(new Student("Maria"));						 // f�gt Person an Stelle ein, wo Iterator ist
		i.add(new Student("Nena"));

		System.out.println(i.next()); 						// Peter

		i.remove(); 										// entfernt Peter

		System.out.println();

		while (i.hasNext())
		{ 													// gibt nur Max aus, weil Iterator dort ist und max das letzte Element ist
			System.out.println(i.next().getName());
		}
		System.out.println();

		i = studenten.listIterator();
		while (i.hasNext())
		{ 													// gibt alle aus, weil Iterator neu instanziert wurde
			System.out.println(i.next().getName());
		}

		Student s5 = new Student("Hans", 22);
		Student s6 = new Student("Hans", 15);
		
		System.out.println(s5.equals(s6));
		
		System.out.println(s5.equals(s6.getName()));
		
	}
	
	

}
