package org.campus.personen;

public class Gehaltskonto
{
	private String kontoinhaber;
	private double kontostand;
	private String iban;
	private String bic;
	
	
	public Gehaltskonto (String kontoinhaber, String iban, String bic)
	{
		this.kontoinhaber=kontoinhaber;
		this.iban=iban;
		this.bic=bic;
		kontostand=0;
	}
	

	public void setInhaber(String kontoinhaber) // Methode weist Name des Inhabers zu und setzt Kontostand auf 0. Kontoinhaber ist nun zugewiesen.
	{
		this.kontoinhaber = kontoinhaber; // ich kann auf Kontoinhaber von au�en zugreifen, weil er zu Variable zugewiesen wurde. Kontostand wurde nicht zugewiesen. 
		this.kontostand = 0;
	}


	public double aufbuchen(double betrag) 
	{
		this.kontostand += betrag;
		return kontostand;
	
	}
	
	public double getKontostand ()  // damit ich von au�en abfragen kann, wie hoch mein Kontostand ist, brauche ich get. F�r einzelne private Variablen.
	{
		return this.kontostand; 	}
	

	public double abbuchen(double betrag)
	{
		kontostand = betrag < kontostand ? kontostand - betrag : kontostand ; // verk�rzte if-Schleife
			// wenn betrag kleiner als kontostand? if true: kontostand -betrag. if false: konstostand.
		
		return kontostand;
		
//		if (this.kontostand - betrag > 0)
//		{
//			System.out.println("Sie k�nnen max." + kontostand +" � abbuchen.");
//		
//			this.kontostand -= betrag;
//			System.out.println("Sie haben "+ betrag + " � abgebucht.");
//			
//		} else
//		{
//			System.out.println("Zu wenig Geld am Konto.");
//		}

	}

	public String toString()
	{
		return String.format("Inhaber: %s \n  Kontostand: %.2f � \n", kontoinhaber, kontostand);  //%f = double, %d = digit = zahl, %s=string
	}
}
