package org.campus.personen;

public class Person
{

	private String vorname;
	private String nachname;
	private int alter;
	private String adresse;
	
	
	public Person() {};
	
	public Person (String vorname, String nachname, int alter)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		this.alter = alter;
	}

	public void setAdresse (String adresse)
	{
		this.adresse=adresse;
	}
	
	public String getAdresse ()
	{
		return adresse;
	}


public String toString()
{
//	if (adresse != null)

	
	return String.format("%s %s, %d Jahre, %s", vorname, nachname, alter, adresse); }
}
	
//	public String toString()
//	{
//	return vorname + " " + nachname + "," + alter + "Jahre";
//	}

