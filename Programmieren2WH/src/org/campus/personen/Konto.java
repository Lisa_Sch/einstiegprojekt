package org.campus.personen;

public class Konto
{

	private String kontoinhaber;
	private double kontostand;
	

	public Konto ()
	{
	}
	

	public void setInhaber(String kontoinhaber) // Methode weist Name des Inhabers zu und setzt Kontostand auf 0. Kontoinhaber ist nun zugewiesen.
	{
		this.kontoinhaber = kontoinhaber; // ich kann auf Kontoinhaber von au�en zugreifen, weil er zu Variable zugewiesen wurde. Kontostand wurde nicht zugewiesen. 
		this.kontostand = 0;
	}


	public void aufbuchen(double betrag) 
	{
		this.kontostand += betrag;
	
	}
	
	public double getKontostand ()  // damit ich von au�en abfragen kann, wie hoch mein Kontostand ist, brauche ich get. F�r einzelne private Variablen.
	{
		return this.kontostand; 	}
	

	public void abbuchen(double betrag)
	{
		kontostand = betrag < kontostand ? kontostand - betrag : kontostand; // verk�rzte if-Schleife
			// wenn betrag kleiner als kontostand? if true: kontostand -betrag. if false: konstostand.
		
//		if (this.kontostand - betrag > 0)
//		{
//			System.out.println("Sie k�nnen max." + kontostand +" � abbuchen.");
//		
//			this.kontostand -= betrag;
//			System.out.println("Sie haben "+ betrag + " � abgebucht.");
//			
//		} else
//		{
//			System.out.println("Zu wenig Geld am Konto.");
//		}

	}

	public String toString()
	{
		return String.format("Inhaber: %s \n  Kontostand: %.2f � \n", kontoinhaber, kontostand);  //%f = double, %d = digit = zahl, %s=string
	}
	
	public static int summe (int a, int b) //  static Methode existiert nur einmal pro Klasse. Auch Variablen k�nnen static sein. Wenn wir static �berschreiben, 
											// �berschreiben wir es �berall.
	{
		int summe;
		return summe = a+b;
	}
}
