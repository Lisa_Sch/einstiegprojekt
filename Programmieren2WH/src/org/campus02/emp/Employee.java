package org.campus02.emp;

public class Employee
{
	private int empNumber;
	private String name;
	private double salary;
	private String departement;

	public Employee(int empNumber, String name, double salary, String departement)
	{
		this.empNumber = empNumber;
		this.name = name;
		this.salary = salary;
		this.departement = departement;
	}

	public int getEmpNumber()
	{
		return empNumber;
	}

	public String getName()
	{
		return name;
	}

	public double getSalary()
	{
		return salary;
	}

	public String getDepartemet()
	{
		return departement;
	}

	public void setSalary(double salary)
	{
		this.salary = salary;
	}

	public void setDepartement(String dep)
	{
		this.departement = dep;
	}

	public String toString()
	{
		return String.format("Nummer: %d, Name: %s\nGehalt: %.2f, Abteilung: %s \n", empNumber, name, salary, departement);
	}
}
