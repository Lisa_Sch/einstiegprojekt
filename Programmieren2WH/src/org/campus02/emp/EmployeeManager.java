package org.campus02.emp;

import java.util.ArrayList;

public class EmployeeManager
{
	private ArrayList<Employee> employees = new ArrayList<Employee>();

	public void addEmployee(Employee emp)
	{
		employees.add(emp);
	}

public Employee findByMaxSalary ()
{
	
	Employee result=employees.get(0);
	
	for (int index =0; index < employees.size(); index++)
	{
		if (result.getSalary() < employees.get(index).getSalary())
		{
			result = employees.get(index);
		}
		
	}
	return result;
}
	
	public Employee findByEmpNumber (int number)
	{
	
		
		for (Employee employee : employees)
		{
			if (employee.getEmpNumber() == number)
			{
				return employee;	
		
		} 
		}
		return null;
		
		
}
	
	public void findByDepartement (String dep)
	{
		ArrayList<Employee>departementList = new ArrayList<Employee>();
		
		for (Employee employee : employees)
		{
			if (employee.getDepartemet().equals(dep))
			{
				departementList.add(employee);
			}
		}
		System.out.println("Mitarbeiter der gleichen Abteilung:  " + departementList);
	}

}
