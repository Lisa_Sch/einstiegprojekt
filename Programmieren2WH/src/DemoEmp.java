import org.campus02.emp.Employee;
import org.campus02.emp.EmployeeManager;

public class DemoEmp
{

	public static void main(String[] args)
	{
		Employee emp1 = new Employee(2, "hans", 200, "bla");
		Employee emp2 = new Employee(3, "fritz", 700, "hui");
		Employee emp3 = new Employee(4, "fritz", 800, "bla");

		EmployeeManager manager1 = new EmployeeManager();

		manager1.addEmployee(emp1);
		manager1.addEmployee(emp2);
		manager1.addEmployee(emp3);

		System.out.println("Mitarbeiter mit dem h�chsten Gehalt: \n" + manager1.findByMaxSalary());

		System.out.println(manager1.findByEmpNumber(2));
		System.out.println(manager1.findByEmpNumber(5));

		manager1.findByDepartement("bla");

		Employee emp4 = new Employee(1, "susi", 900, "bla");

		manager1.addEmployee(emp4);
		System.out.println(manager1.findByEmpNumber(3));

	}

}
