
public class Matrix
{

	public static void main(String[] args)
	{

		double matrix1[][] = generateIdentityMatrix(5); // Erste [] sind Zeilen und in jeder Zeile ist weiteres Array (zweite [])

		printMatrix(matrix1);
	}

	public static double[][] generateIdentityMatrix(int size)
	{
		double[][] matrix = new double[size][size];

		for (int i = 0; i < matrix.length; i++)
		{
			matrix[i][i] = 1.0; // der Wert 1.0 soll Diagonal im Array ausgegeben werden
								// (0.0/1.1/2.2/3.3/4.4/5.5)
		}

		return matrix;
	}

	public static void printMatrix(double[][] matrix)
	{
		for (int i = 0; i < matrix.length; i++) // i ist zu Beginn 0, h�pft in die zweite Schleife. Print alle Stellen
												// in erster Zeile (i=0, a=0/1/2/3/4)
		{
			for (int a = 0; a < matrix[i].length; a++) // wenn a =4 -> Zeile ist fertig, h�pft aus der Schleife in die
													// obere -> i =1 -> zweite Zeile
			{
				System.out.print(matrix[i][a] + "\t");
			}
			System.out.println();
		}

	}
}
