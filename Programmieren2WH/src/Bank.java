import java.util.ArrayList;

import org.campus.personen.Gehaltskonto;

public class Bank
{

	public static void main(String[] args)
	{
		Gehaltskonto konto1 = new Gehaltskonto("Hans", "iban1", "bic1");
		
		Gehaltskonto konto2 = new Gehaltskonto("Lara", "iban2", "bic2");

		Gehaltskonto konto3 = new Gehaltskonto("Susi", "iban3", "bic3");
	
	
	konto1.aufbuchen(100);
	konto2.aufbuchen(550);
	konto3.aufbuchen(300);
	
	System.out.println(konto1);
	System.out.println(konto2);
	System.out.println(konto3);
	
	konto2.abbuchen(10);
	konto3.abbuchen(80);
	konto1.abbuchen(200);
	
	System.out.println(konto2);
	System.out.println(konto3);
	System.out.println(konto1);
	
	ArrayList<Gehaltskonto>kontoliste = new ArrayList<Gehaltskonto>();
	
	kontoliste.add(konto1);
	kontoliste.add(konto2);
	kontoliste.add(konto3);
	
	for (Gehaltskonto gehaltskonto : kontoliste)
	{
		System.out.println(gehaltskonto.getKontostand());
	
	}
	
		
		

}
}
