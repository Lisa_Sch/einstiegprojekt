package at.campus02.pr3.netzwerk;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class UDPClient
{

	public static void main(String[] args) throws IOException
	{
		DatagramSocket client = new DatagramSocket();

		String input = "abcde";									// Daten, die verschickt werden sollen
			
		byte[] sendData = new byte[1024];						// Platzhalter f�r die zu versendenden Daten / Daten, die empfangen werden 
		byte[] receiveData = new byte[1024];
		
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getLocalHost(), 1111);		
		// leeres Paket wird erstellt, das versendet werden soll -> sendet auf localhost auf Port 1111 (Port vom Server)
		
		sendPacket.setData(input.getBytes());					// Daten in leeres Paket schreiben
		
		client.send(sendPacket);								// Client versendet Paket 
		
		DatagramPacket receivePaket = new DatagramPacket(receiveData, receiveData.length); // leeres Paket f�r empfangene Daten wird erstellt 
		
		client.receive(receivePaket); 							// Client empf�ngt Paket und receivePaket wird mit Daten vom Server bef�llt
		
		String s = new String(receivePaket.getData());			// Daten aus Paket in String gespeichert 
		
		System.out.println("Reply from Server: " + s);
		
		
		
	}

}
