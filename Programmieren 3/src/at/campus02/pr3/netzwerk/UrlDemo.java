package at.campus02.pr3.netzwerk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class UrlDemo
{

	public static void main(String[] args)
	{
		try
		{
			URL url = new URL("http://orf.at");
			
			// Url hat Methode openStream(kommt in bytes), ich m�chte aber Zeichen haben -> �bersetzen in inputStreamReader und zeilenweise (BufferedReader)
		
			
		try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {		// BufferedReader um zeilenweise einzulesen, weil http-Dokument
			
			String line;																			// InputStreamReader, weil bufferdReader einen Reader braucht 
																									// StreamReader braucht Stream
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}
		
			
		}
		
			
		} catch (MalformedURLException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
