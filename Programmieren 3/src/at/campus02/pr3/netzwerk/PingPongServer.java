package at.campus02.pr3.netzwerk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class PingPongServer
{

	public static void main(String[] args)
	{

		try
		{

			ServerSocket server = new ServerSocket(1111); // Server anlegen und Port gleich angeben

			while (true)
			{

				Socket socket = server.accept(); // Server wartet - wenn Client kommt, wird Socket erstellt

				System.out.println("Accepted: " + socket);

				// serverSocket.bind(arg0); Server an IP-Adresse und Port binden

				try (

						InputStream ins = socket.getInputStream();

						BufferedReader br = new BufferedReader(new InputStreamReader(ins)); 				// um Input von Client einzulesen

						PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));) // um Antwort an Client zu schicken
				// PrintWriter schreibt zeilenweise und sendet mit flush ab
				// BufferedOutputWriter macht keine Umbr�che
				{

					String line;

					while ((line = br.readLine()) != null)
					{
						System.out.println("Received: " + line);

						switch (line.toLowerCase())
						{
						case "ping":
							pw.println("pong"); // schickt Antwort an Client zur�ck  
							break;
						case "pong":
							pw.println("ping");
							break;

						default:
							pw.println("what?");
							break;
						}

						pw.flush();

					}
				}
			}
		} catch (SocketException e)
		{
			System.out.println("Client disconnected.");
		}

		catch (IOException e)
		{
			e.printStackTrace();

		}
	}
}
