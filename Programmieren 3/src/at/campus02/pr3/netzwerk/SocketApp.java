package at.campus02.pr3.netzwerk;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;





public class SocketApp
{

	public static void main(String[] args) throws UnknownHostException, IOException
	{
		
		
		Socket socket = new Socket("wetter.orf.at", 80);					// Socket wird erstellt (Socket brauche ich f�r Verbindungsaufbau zu Server)
																			// Ich kommuniziere mit Server von orf.at 

		OutputStream os = socket.getOutputStream();							// Ich m�chte Request schicken
		
		String requestLine1 = "GET /steiermark/prognose HTTP/1.1\r\n";
		String requestLine2 = "Host: orf.at\r\n\r\n";
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
		
		bw.write(requestLine1);												// Ich �bergebe meinem Writer meinen Request
		bw.write(requestLine2);												
		
		bw.flush(); 														// wenn fertig geschrieben, sendet bw Strings ab 
		
		InputStream ins = socket.getInputStream();							// socket empf�ngt Daten von Server, weil Stream und ich in Zeilen lesen will -> BufferedReader
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
		
		String line;
		
		while((line = reader.readLine()) != null) {
			System.out.println(line);
		}
		
		
		socket.close();
		
	}

}
