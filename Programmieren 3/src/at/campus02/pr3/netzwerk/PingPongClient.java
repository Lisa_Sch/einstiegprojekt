package at.campus02.pr3.netzwerk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class PingPongClient			// Ping-Pong-Bsp = Pr�fungsBsp
{

	public static void main(String[] args) throws UnknownHostException, IOException
	{
		Socket clientSocket = new Socket("localhost", 1111); 												// damit Client wei�, wo Anfrage hinschicken - IP-Adresse und Port angeben
		 
		
		try (
		
		BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));				// von Konsole einlesen 
		
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));			// um Antwort an Server zu schreiben 
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));	// um Antwort von Server einzulesen 
		) {
		
			
		String line;															// BufferedWriter wartet bei Eingabe auf Zeilenumbruch, PrintWriter nicht 
		
		while((line = consoleReader.readLine()) != null) {
			
			if (line.equalsIgnoreCase("exit")) {
				break;
			}
			
			pw.println(line); 												// User schreibt in Console, Reader liest aus, PrintWriter schickt Zeile an Server (wartet nicht auf Zeilenumbruch)
			
			pw.flush(); 													// Nachricht bewusst abschicken, auch wenn Paket (vorgegebene 1550 bytes) noch nicht voll ist  
			
			System.out.println("Antwort vom Server: "+ reader.readLine());	// reader liest Antwort von Server
		}
		
		

		}
	}

}
