package at.campus02.pr3.netzwerk;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class UDPServer
{

	public static void main(String[] args) throws IOException
	{
		DatagramSocket server = new DatagramSocket(1111);					// Server horcht auf Port 1111 und wartet auf Pakete, Sever erstellen (Ports ab 1024 m�glich)
		
		
		while (true) {														// endlos-Schleife, damit Sever immer arbeitet wenn Paket kommt 
			
		byte[] receiveData = new byte[1024];								// leeres Paket als Platzhalter			
		
		byte[] sendData = new byte[1024];					
		
		DatagramPacket receivePaket = new DatagramPacket(receiveData, receiveData.length);		// Server erh�lt Pakete vom Client
		
		
		server.receive(receivePaket); 										// Client schickt Paket, Server empf�ngt und  Paket wird bef�llt
		
		byte[] received = receivePaket.getData();
		String s = new String(received);									// aus byte String machen, das was Server vom Client empfangt (Daten aus Paket lesen)
		InetAddress senderAdress = receivePaket.getAddress();				// Adresse des Paketes holen
		int senderPort = receivePaket.getPort();							// Die Info brauche ich, damit ich wei�, wohin ich Paket zur�ckschicken muss
		
		String toSend = s.toUpperCase();
		
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, senderAdress, senderPort);	// neues leeres Paket mit Adresse und Port wird vorbereitet 
		
		sendPacket.setData(toSend.getBytes());								// Paket wird bef�llt				
		
		server.send(sendPacket);											// Server sendet Paket zur�ck
		
		}

	}

}
