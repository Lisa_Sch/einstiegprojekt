package at.campus02.pr3.fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class InputStreamDemo
{

	public static void main(String[] args)
	{
		File f = new File("test.txt");
		
		if (!f.exists()) {
			try
			{
				f.createNewFile();
			} catch (IOException e)
			{
				System.out.println("Fehler beim Erstellen der Datei: "+ e.getMessage());					
				return;
			}
		}
		
		try
		{
			InputStream inp = new FileInputStream(f);			// �ffnet Datei, um sie einzulesen
			
			long length = f.length();
			System.out.println("Die L�nge der Datei: "+ length + " Byte");
			
			inp.skip(3);								// ersten 3 Bytes �berspringen 
			
			// Klassischer Aufbau vom Einlesen einer Datei 
			
			int b;
			while ((b=inp.read()) != -1) {					// solange Ergebnis nicht -1 ist (-1 hei�t, datei ist leer), auslesen. 
				System.out.println(b);						// int zur�ck = Wert von jedem Byte, nachschauen in Ascii-Tabelle welcher Buchstabe 
				System.out.println(Character.toChars(b));	// macht aus int einen Buchstaben 
			}
			
			
			inp.close();
			
		} catch (FileNotFoundException e)
		{		
			System.out.println("Datei wurde nicht gefunden: " + e.getMessage());		// Wenn Datei nicht existiert, kann ich sie nicht �ffnen 
			return; 																	// Programm beenden, weil ohne Datei ich nicht weiterarbeiten kann
		
		} catch (IOException e)
		{
			System.out.println("Datei konnte nicht geschlossen werden: "+ e.getMessage());
			return;
		}			

	}

}
