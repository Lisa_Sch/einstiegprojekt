package at.campus02.pr3.fileio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

public class utfWriter
{

	public static void main(String[] args)
	{
		try (
				OutputStream ou = new FileOutputStream("umlaute.txt");
				Writer ow = new OutputStreamWriter(ou, StandardCharsets.ISO_8859_1);				// nur der kann sagen mit welchem Code er schreiben soll. Kann FileWriter nicht. 
				) {
			
			
			ow.write("K�che machen M�sli mit �pfeln");
			
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
