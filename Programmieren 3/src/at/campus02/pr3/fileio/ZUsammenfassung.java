package at.campus02.pr3.fileio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;

public class ZUsammenfassung {

	public static void main(String[] args) {

		// ZEILENWEISE ZEICHEN EINLESEN AUS CONSOLE

		String line;
		try (BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));) {
			while ((line = br2.readLine()) != null) {
				switch (line) {
				case "STOP":
					return;
				case "HELP":
					System.out.println("STOPP stoppt die Anwendung.");
					break;
				}

				if ((line = br2.readLine()).equals("STOP"))
					return;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		
		
		// ZEILENWEISE ZEICHEN SCHREIBEN IN CONSOLE

		try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(System.out)); // ODER IN DATEI SCHREIBEN
		) {
			pw.println("Test");  // macht Umbrüche
			pw.flush();
		}

		
		
		// BYTES EINLESEN AUS DATEI

		File f = new File("test.txt");

		try (InputStream inp = new FileInputStream(f);
				// Wenn zeilenweise einlesen -> noch einen BufferedInputStream herum 

		) {

			inp.skip(3); // ersten 3 Bytes überspringen

			int b;
			while ((b = inp.read()) != -1) { 
				System.out.println(b);
				System.out.println(Character.toChars(b)); 
			}

		} catch (FileNotFoundException e) {
			System.out.println("Datei wurde nicht gefunden: " + e.getMessage()); 
			return; 

		} catch (IOException e) {
			System.out.println("Datei konnte nicht geschlossen werden: " + e.getMessage());
			return;
		}

		
		// BYTES IN DATEI SCHREIBEN 
		
		String text = "Das ist mehr Text. Juhuuuuu!!!";
		
		try (
				OutputStream out = new FileOutputStream("output.txt");	// macht keinen Umbruch
				PrintStream ps = new PrintStream("output1.txt");  		// macht nach jedem char/zeile einen Umbruch
				)
		{
			
			for (char x : text.toCharArray()) {							
				out.write(x);
			}
			
			for (char x : text.toCharArray()) {							
				ps.println(x);
			}
			
			out.flush();
			ps.flush(); 						
			
			
		} catch (FileNotFoundException e)		
		{										
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
