package at.campus02.pr3.fileio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class ReaderDemo 				// EINLESEN AUS CONSOLE UND ZEILENWEISE ZEICHEN AUSGEBEN 
{

	public static void main(String[] args)
	{
	
//		Reader isr = new InputStreamReader(System.in);					// Reader ist allgemeiner, lasst mehr Möglichkeiten offen 
		
//		BufferedReader br = new BufferedReader(isr);					// BufferedReader braucht als Übergabe einen InputReader: entweder Datei oder Konsoleneingabe, speicher zwischen
		
		
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		
		String line;
		try
		{
			while ((line = br2.readLine()) != null) {					// Ich brauche BufferedReader, weil der die Methode readLine hat (liest zeilenweise ein)
				
				switch (line)
				{
				case "STOP":
					return;
				case "HELP":
					System.out.println("STOPP stoppt die Anwendung.");
					break;
				}
			
				if ((line = br2.readLine()).equals("STOP")) 
					return;
			}
			
		br2.close();
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		

	}

}
