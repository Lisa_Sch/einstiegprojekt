package at.campus02.pr3.fileio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class TryWithResourcesBsp
{

	public static void main(String[] args)
	{
		try (
		Reader fr = new FileReader("noten.csv");				// durch die Schreibweise schlie�t er Ressourcen automatisch -> ich brauch kein close 
		BufferedReader br = new BufferedReader(fr);
		) {
			
		String line;
		
		while((line = br.readLine()) != null) {
			System.out.println(line);
		}
		
		}  catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
