package at.campus02.pr3.fileio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class InputOutputBsp
{

	public static void main(String[] args)
	{
		String text = "Dies ist ein Java-Objekt.";
		
		try
		{
			FileOutputStream fos = new FileOutputStream("object.dat");
			ObjectOutputStream ous = new ObjectOutputStream(fos);
			
			ous.writeObject(text);
			
			ous.flush(); 						
			ous.close();
			
			
			FileInputStream fis = new FileInputStream("object.dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			String text_read = (String) ois.readObject();
			
			ois.close();
			
			System.out.println(text.equals(text_read));
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
