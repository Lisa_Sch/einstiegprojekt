package at.campus02.pr3.fileio;

import java.io.File;
import java.io.IOException;

public class KlasseFile
{

	public static void main(String[] args) {

		
		File f = new File("test.txt");					// Dateinamen f�r Datei vergeben
		f.canExecute();									// Kann ich Datei ausf�hren?
		f.canRead();									// Kann ich Datei lesen?
		f.canWrite();									// Kann ich Datei bearbeiten?
		//f.createNewFile();								// Erstellt Datei 
		f.delete();										// L�scht Datei
		f.deleteOnExit();                               // l�scht Datei beim Schlie�en
		f.getAbsoluteFile();							// gibt neues Fileobjekt mit absolutem Pfad zur�ck
		f.getAbsolutePath();							// gibt asboluten Pfad der Datei zur�ck
		
		System.out.println(f.getAbsolutePath());
		
		if (!f.exists()) {
			try
			{
				f.createNewFile();
			} catch (IOException e)
			{
				System.out.println("Fehler beim Erstellen der Datei: "+ e.getMessage());					// Wir k�nnen Fehler nicht behandeln, deshalb Programmabbruch 
				return;
			}
		}
		
		f.getFreeSpace(); 	// gibt Speicherplatz, die die Datei zur Verf�gung bekommt 
		f.getParent();		// Ordnernamen der Datei
		f.getTotalSpace();	// Wie gro� ist Festplatte, wo Datei liegt 
		f.length();			// Dateigr��e
		
		System.out.println();
		
		File v = new File(".");					// . = Verzeichnis, in dem ich mich gerade befinde
		
		
		if (v.isDirectory()) {
			for (String s : v.list())
			{
				System.out.println(s); 			// gibt Dateien in dem Verzeichnis aus 
			}
		}
		
		System.out.println();
		
		File verz = new File("./bla/blu/blo/"); 		// Wenn ich nicht wei�, welches Trennzeichen -> / (Linux) -> JVM macht richtiges daraus 
		
		if (!verz.exists()) {
			System.out.println(verz.mkdirs());	// mkdir legt nur ein Verzeichnis an. mkdirs legt verschachtelte Verzeichnisse an.
			
		}
		
//		f.renameTo(dest); 					// bennent Datei um und verschiebt Datei wenn gew�nscht 
		
		System.out.println(f.toURI()); 		// Aus lokalem Dateipfad eine Url machen 
		
		System.out.println(f.pathSeparator); 	// Verzeichnisse werden in Windows mit ; getrennt 
		System.out.println(f.separator); 		// Verzeichnisnamens-Trenner in Windows		--> damit Java auf jedem Rechner die richtigen Zeichen verwendet 
		
	}

}
