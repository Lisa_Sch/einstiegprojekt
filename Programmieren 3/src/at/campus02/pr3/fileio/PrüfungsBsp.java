package at.campus02.pr3.fileio;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Pr�fungsBsp
{

	public static void main(String[] args)
	{
		
		// Von Konsole Text einlesen ohne FileReader  -> dann nur mehr inputStream zur Verf�gung

		try
		{
			InputStream ins = new FileInputStream("noten.csv");									// jetzt k�nnen wir nur bytes einlesen = Stream
			
			// Damit ich zeilenweise einlesen kann, brauche ich bufferedReader (readLine)
			
			InputStreamReader ir = new InputStreamReader(ins);									// liest bytes ein und gibt Text weiter = Reader
			
			BufferedReader br = new BufferedReader(ir);			
			
			br.readLine(); 																		// zeilenweises Einlesen von Text m�glich  
			
			
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		
		
		
		// In Datei zeilenweise schreiben ohne FileWriter
		
		try
		{
			OutputStream os = new FileOutputStream("noten.csv");								// schreibt bytes in Dokument 
			
			OutputStreamWriter ow = new OutputStreamWriter(os);									// �bersetzt Strings in bytes, reicht diese an os weiter 
			
			ow.write("Zeile");
			
			
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
