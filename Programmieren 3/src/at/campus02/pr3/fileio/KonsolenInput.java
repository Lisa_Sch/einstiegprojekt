package at.campus02.pr3.fileio;

import java.io.IOException;

public class KonsolenInput
{

	public static void main(String[] args)
	{
		
		int b;
		
		try
		{
			
			
			System.out.println("Ihre Eingabe:");

			while ((b=System.in.read()) != -1) {
				if (b == 88 || b==120) {
					return;
				} else {
					System.out.println(Character.toChars(b));
				}
				
			}
		} catch (IOException e)
		{
			System.out.println("Datei konnte nicht gelesen werden: "+ e.getMessage());
		}
		
		
		try
		{
			
			int p; 
			System.out.println("Ihre Eingabe:");

			while ((p=System.in.read()) != -1) {
				
				char c = Character.toChars(p)[0];
				
				if (c == 'x' || c == 'X') {
					return;
				}  else 
					System.out.print(c);
				
			}
		} catch (IOException e)
		{
			System.out.println("Datei konnte nicht gelesen werden: "+ e.getMessage());
		}

	}

}
