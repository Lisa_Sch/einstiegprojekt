package at.campus02.pr3.fileio;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class EncryptionOutputStream extends FilterOutputStream	 			// Abstrakte Klasse 
{
	
	private int key;

	public EncryptionOutputStream(OutputStream out, int key)
	{
		super(out);
		this.key=key;
	}

	@Override
	public void write(int b) throws IOException
	{
		super.write(b+key); 								// Ich z�hle zu dem byte eins dazu und der gibt es an bos weiter, welcher es durch reicht an fos - dazuz�hlen in ascii table
	}
	
	
	

}
