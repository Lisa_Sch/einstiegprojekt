package at.campus02.pr3.fileio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;

public class FileWriterBsp														// Von Konsole einlesen: System.in
{																				// Lesen Sie zeilenweise ein: InputStreamReader
																				// Lesen Sie Bytes ein: InputStream
																				// Schreiben Sie Strings in eine Datei: FileWriter
																				// OutputStreamWriter: schreibt in OutputStream, wenn FileWriter nicht benutzt werden darf
	public static void main(String[] args)
	{
		InputStreamReader isr = new InputStreamReader(System.in);				// User gibt Daten in Konsole ein bis STOP				
		
		BufferedReader br = new BufferedReader(isr);							// Speichert Daten zwischen
				
		
		String line;
		
		try
		{
			Writer fw = new FileWriter("noten.csv");							// schreibt bytes in Zeichen direkt in Datei 
			while ((line = br.readLine()) != null) {					
				
				
				for (String i : line.split(" ")) {
					
					if (i.length() == 0) {
						continue;												// �berspringe dieses Element 
					}															// break = beende die dem break am n�chsten gelegene Schleife 
					
					if (line.equals("STOP")) {
						fw.close();
						return;
					}
					
					if (i.charAt(i.length()-1) == ':') {
						fw.write(i.substring(0, i.length()-1));
							
						} else {
							fw.write(i);
						}
					
						fw.write(";");											// typisch f�r csv Dateien, um Elemente zu trennen
		
					}
						fw.write("\r\n");										// Umbruch einf�gen, PrintWriter w�rde dies automatisch machen 
					
					fw.flush();		
					}
	
			
		
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		

	}

	}


