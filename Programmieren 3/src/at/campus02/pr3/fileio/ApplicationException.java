package at.campus02.pr3.fileio;

import java.io.File;
import java.io.IOException;

public class ApplicationException
{

	public static void main(String[] args) 
	{
//		try
//		{
//			System.in.available();						// schaut, ob noch Byte gelesen werden kann
//			System.in.read();							// liefert int zur�ck von Byte, das ich gerade gelesen habe - liest ein Byte ein, Cursor zeigt immer auf n�chstes Byte
//			System.in.read();							// liefert array von Bytes zur�ck, gibt int zur�ck, wie viele Bytes bef�llt werden konnten 
//			System.in.skip();							// nimm Cursor, wo er gerade steht und �berspringe soviele Bytes
//			System.in.reset();                          // schiebt Cursor wieder zum Anfang zur�ck
//			System.in.close();                          // schlie�t Datei 
//			
//		} catch (IOException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		File f = new File("test.txt");					// Dateinamen f�r Datei vergeben
		f.canExecute();									// Kann ich Datei ausf�hren?
		f.canRead();									// Kann ich Datei lesen?
		f.canWrite();									// Kann ich Datei bearbeiten?
//		f.createNewFile();								// Erstellt Datei 
		f.delete();										// L�scht Datei
		f.deleteOnExit();                               // l�scht Datei beim Schlie�en
		f.getAbsoluteFile();							// gibt neues Fileobjekt mit absolutem Pfad zur�ck
		f.getAbsolutePath();							// gibt asboluten Pfad der Datei zur�ck
		
		System.out.println(f.getAbsolutePath());
		
//		if (!f.exists()) {
//			try
//			{
//				f.createNewFile();
//				throw  new ExceptionBsp();			// dort ist ein Fehler aufgetreten = throw -> Ich werfe es an n�chst h�here Methode, die Exception f�ngt. Sonst ganz nach oben in JVM.
//			} catch (IOException e)					// F�r jeden Exceptiontyp einen eigenen Catch-Block, damit ich wei�, welche Exception aufgetreten ist.
//			{
//				System.out.println("Fehler bei der Datei.");
//				ExceptionBsp bsp = new ExceptionBsp(); 		// Exception wird unterdr�ckt und wird in meiner eigenen Exception weiter nach oben geworfen, damit Benutzer nicht die eine Exception liest
//				bsp.addSuppressed(e);
//				throw bsp;
//				
//			} catch (ExceptionBsp e)						// Wenn Exception auftritt im Try-Block, dann fang sie im Catch-Block. Dort wird sie behandelt. Wenn Try-Catch vorhanden. 
//			{
//				System.out.println("Wir haben einen Fehler festgestellt.");		// Wenn meine Funktion den Fehler nicht behandeln kann, muss ich Fehler weiter rauf werfen und keinen Catch-Block.
//				
//			}
			
			// Catch-Block: Meldung ausgeben aber Programm nicht gleich abbrechen - Vorgang wiederholen lassen 
		}
		

	}


