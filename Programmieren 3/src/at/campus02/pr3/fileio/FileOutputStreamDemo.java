package at.campus02.pr3.fileio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileOutputStreamDemo
{

	public static void main(String[] args)
	{
		String text = "Das ist mehr Text. Juhuuuuu!!!";
		
		try
		{
			OutputStream out = new FileOutputStream("output.txt");
			
			for (char x : text.toCharArray()) {							// char kann sich in int casten 
				out.write(x);
			}
			
			out.flush(); 						// blockiert Ausf�hrung solange, bis Betriebssystem sagt, dass alle Daten auf der Festplatte sind
			out.close();
			
		} catch (FileNotFoundException e)		// Wenn ich in bestehende Datei am Ende des Inhaltes etwas dazuschreiben m�chte, 
		{										// muss ich zuerst den Cursor ganz nach hinten setzen (mit File-Klasse), sonst �berschreibe ich Inhalt 
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
