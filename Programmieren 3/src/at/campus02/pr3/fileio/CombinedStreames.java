package at.campus02.pr3.fileio;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CombinedStreames
{

	public static void main(String[] args)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream("combinedstreams.txt");
			
			BufferedOutputStream bos = new BufferedOutputStream(fos); 		// braucht OutputStream als Argument. �bergibt Inhalte an fos.
			
			EncryptionOutputStream eos = new EncryptionOutputStream(bos,5); 	// �bergibt Inhalte an bos.
			
			eos.write('a');
			eos.write('b');
			eos.write('c');
			
			eos.flush();
			
			eos.close();
			
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
