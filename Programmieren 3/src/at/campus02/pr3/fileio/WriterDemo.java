package at.campus02.pr3.fileio;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;


public class WriterDemo
{

	public static void main(String[] args)
	{
		Writer ow = new OutputStreamWriter(System.out);			// System.out ist Klasse PrintWriter - hat Methode system.out.println schon, die zeilenweise schreibt 
		
		BufferedWriter bw = new BufferedWriter(ow);				// zum Zwischenspeichern 
		
		try
		{	
			bw.write("Dies ist ein Text\r\n");					// BufferedReader kennt nur Methode write ohne Zeilenumbruch 
			bw.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		PrintWriter pw = new PrintWriter(ow);					// Methode println macht automatisch Zeilenumbruch 
		pw.println("Test");
		pw.flush();
		
		

	}

}
