package at.campus02.pr3.quizduell.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import at.campus02.pr3.quizduell.messages.Answer;
import at.campus02.pr3.quizduell.messages.Player;
import at.campus02.pr3.quizduell.messages.Question;

public class Client
{

	public static void main(String[] args)
	{
		try (
				Socket socket = new Socket("localhost", 1111);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());			// zuerst oos -> Server wartet auf die Anfrage vom Server 
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				
				) {
			
			System.out.println("Bitte Spielernamen eingeben:");
			String name = br.readLine();
			Player p = new Player(name);
			oos.writeObject(p);
			
			oos.flush();
			
			while (true) {
			Question q = (Question)ois.readObject();
			
			System.out.println(q.getText() + " (Schwierigkeitsgrad: " + q.getLevel() + " )");
			
			int counter = 1;
			HashMap<Integer, UUID> prefixMap = new HashMap<>();
			
			for (Entry<UUID, String> entry : q.getAnswers().entrySet()) {
				
				System.out.println("   " + counter + ": " + entry.getValue());			// Benutzer sieht Antwortm�glichkeit mit 1,2,3,4 
				
				prefixMap.put(counter, entry.getKey());									// Benutzer gibt Zahl ein, damit ich wei�, welche Zahl = welche Frage -> HashMap
				
				counter++;
			}
			
			
			System.out.println("Bitte geben Sie Ihre Antwort (1 / 2 / 3 / 4) ein:");
			
			int id;
			String answer;
			
			 while (true) {
				 answer = br.readLine();
				 if (answer == null ) {
					 System.out.println("Auf Wiedersehen, selber schuld.");
					 return;
				 } 
				 try {
					id = Integer.parseInt(answer);
					if (prefixMap.containsKey(id)) {
						break;															// Wenn User-Eingabe eine Zahl ist und in HashMap enthalten ist -> Schleife beenden
					} else {
						System.out.println("Ung�ltige Eingabe.");
					}
				 } catch(NumberFormatException e) {
					 System.out.println("Ung�ltige Eingabe.");
				 }
				 
			 }
			 
			 Answer a = new Answer(prefixMap.get(id)); 							// Ich bekomme f�r User-Eingabe (int id) eine UUID f�r Antwort zur�ck und �bergebe sie der Antwort
			 
			 oos.writeObject(a);
			 oos.flush();
			 
			 Answer as = (Answer) ois.readObject();								// Server schickt richtige Antwort 
			 
			 if(as.getGuess().equals(prefixMap.get(id))) {						// Vergleich UUID der richtigen Antwort mit UUID von Zahl-UUID-Map, die vom User eingegeben wurde
				 System.out.println("Gratulation!");
			 } else {
				 System.out.println("Falsche Antwort - die richtige w�re gewesen: " + q.getAnswers().get(as.getGuess())); // richtige Antworten-Text mit UUID raus holen
			 }
			
			if(q.isLast()) {													// Wenn isLast auf true -> while-Schleife im Client beenden
				break;
			}
			}
			
			System.out.println("Danke f�r's Mitspielen!");
			
		} catch (IOException e)
		{
			e.printStackTrace();
			
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

}
