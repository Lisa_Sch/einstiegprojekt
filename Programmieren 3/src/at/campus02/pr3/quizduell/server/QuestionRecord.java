package at.campus02.pr3.quizduell.server;

import java.util.ArrayList;

public class QuestionRecord 					// Klasse ist nur f�r Server, um aus Datei einzulesen 
{
	private String text;
	private int level;
	private AnswerRecord correct;
	private ArrayList<AnswerRecord> wrong = new ArrayList<>();
	
	
	public QuestionRecord(String text, int level, AnswerRecord correct) 
	{
		super();
		this.text = text;
		this.level = level;
		this.correct = correct;
	}


	public String getText()
	{
		return text;
	}


	public int getLevel()
	{
		return level;
	}


	public AnswerRecord getCorrect()
	{
		return correct;
	}


	public ArrayList<AnswerRecord> getWrong()
	{
		return wrong;
	}
	
	
	
	
	
	

}
