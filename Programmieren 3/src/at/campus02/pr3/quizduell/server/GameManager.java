package at.campus02.pr3.quizduell.server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;


public class GameManager
{
	private ArrayList<QuestionRecord>questions = new ArrayList<>();
	Random r = new Random();

	public GameManager() {
		super();
		try (
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("quizfragen.csv"), StandardCharsets.UTF_8)); // Fragen aus Datei einlesen 
				) {
			
			String line;
			
			while((line = br.readLine()) != null) {
				
				String[] fields = line.replace("\"", "").split(";"); 			// jede Zeile der Datei wird in Array gespeichert
				if(fields.length != 7) {
					continue; 													// diesen Schleifendurchgang abbrechen und n�chste Zeile einlesen
				}
				
				try {
				Integer.parseInt(fields[6]);
				}
				catch(NumberFormatException e) {
					continue;
				}
				
				QuestionRecord qr = new QuestionRecord(fields[0], Integer.parseInt(fields[6]), new AnswerRecord(fields[1]));
				qr.getWrong().add(new AnswerRecord(fields[2]));
				qr.getWrong().add(new AnswerRecord(fields[3]));
				qr.getWrong().add(new AnswerRecord(fields[4]));					// ein QuestionRecord besteht aus Frage, Level, richtige Antwort, falsche Antworten
				
				questions.add(qr); 												// GameManager verwaltet Liste mit allen Fragen und deren Antworten
				
				
			}
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public synchronized QuestionRecord getRandomQuestion() { 					// holt sich aus Liste der Fragen eine zuf�llige heraus (immer nur ein Spieler kann das machen)
		int random = r.nextInt(questions.size());								// Zufallsindex generieren 
		QuestionRecord q =  questions.get(random);								// speichert gew�hlte Frage an Zufallsindex
		questions.remove(random);												// l�scht Frage an Zufallsindex, damit nicht doppelt gew�hlt
		return q;
	}
}
