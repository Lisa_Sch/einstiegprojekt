package at.campus02.pr3.quizduell.server;

import java.util.UUID;

public class AnswerRecord
{

	private UUID uuid = UUID.randomUUID();				// jede Antwort bekommt eindeutige ID
	private String text;
	
	public AnswerRecord(String text)
	{
		super();
		this.text = text;
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getText()
	{
		return text;
	}
	
	
	
}
