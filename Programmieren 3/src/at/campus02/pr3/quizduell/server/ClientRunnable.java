package at.campus02.pr3.quizduell.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import at.campus02.pr3.quizduell.messages.Answer;
import at.campus02.pr3.quizduell.messages.Player;
import at.campus02.pr3.quizduell.messages.Question;

public class ClientRunnable implements Runnable
{

	// Thread l�uft f�r einen Spieler
	
	private Socket client;
	private GameManager gm;
	
	
	
	
	public ClientRunnable(Socket client, GameManager gm)
	{
		this.client = client;
		this.gm = gm;
	}




	@Override
	public void run()		// ObjectInputStream und ObjectOutputStream von Client und Server kommunizieren miteinander gegengleich 
	{
		try (
				ObjectInputStream ois = new ObjectInputStream(client.getInputStream());				// ois zuerst aufmachen, wartet auf outputstream von client
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
				
				) {
			
		Player p =	(Player) ois.readObject(); 										// Server wartet solange, bis PlayerObjekt geschickt wurde und liest Objekt von Client ein 
		System.out.println(p.getName());
		
		int maxRounds=5;
		int count=1;
		
		while (count <= maxRounds) {
		QuestionRecord qr = gm.getRandomQuestion();									// holt zuf�llige Frage aus GameManager
		
		// �bersetzen von QuestionRecord in Question
		
		Question q = new Question(qr.getLevel(), qr.getText());						// erstellt neue Frage mit Daten von qr von Zufallsfrage 
	
		q.getAnswers().put(qr.getCorrect().getUuid(), qr.getCorrect().getText());	// f�gt zu Zufallsfrage die richtige Antwort und die falschen Antworten dazu
		
		for (AnswerRecord ar : qr.getWrong())
		{
			q.getAnswers().put(ar.getUuid(), ar.getText());
		}
	
		if(count == maxRounds) { 													// Wenn 5 Fragen gesendet -> Frage bekommt Attribut isLast=true = Abbruchbedingung f�r Client 
			q.setLast();
		}
		
		oos.writeObject(q);															// Server schickt ZufallsFrage an Client 
		oos.flush();
		
		Answer a = (Answer) ois.readObject();
		
		boolean correct = a.getGuess().equals(qr.getCorrect().getUuid());			// Vergleich zwischen UUID User-Antwort und Zufallsfrage von vorher mit der richtigen Antwort
			
		Answer as = new Answer(qr.getCorrect().getUuid());							// richtige Antwort mit der UUID zur�ckschicken
		oos.writeObject(as);
		oos.flush();
		
		count++;
		
		}
		
		} catch (IOException e)
		{
			
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		}

	}


