package at.campus02.pr3.quizduell.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultithreadingServer
{

	public static void main(String[] args)
	{
		try
		{
			ServerSocket server = new ServerSocket(1111);
			GameManager gm = new GameManager(); 			
			
			while(true) {
				
				Socket socket = server.accept();						// wartet auf Client, der sich verbinden will
				
				System.out.println("Accepted " + socket);
				
				ClientRunnable cr = new ClientRunnable(socket, gm);		// sobald Verbindung angenommen, wird Thread f�r Verbindung gestartet 
				
				Thread t = new Thread(cr);								// erzeugt einen Thread mit einem Spieler 
				t.start();												// run() im ClientRunnable wird gestartet 
			
			
			}
			
		} catch (IOException e)
		{
			
			e.printStackTrace();
		}

	}

}
