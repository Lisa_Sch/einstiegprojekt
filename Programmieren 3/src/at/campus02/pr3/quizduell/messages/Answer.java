package at.campus02.pr3.quizduell.messages;

import java.io.Serializable;
import java.util.UUID;

public class Answer implements Serializable
{
	private static final long serialVersionUID = 3L;			// Damit Server wei�, welche Klasse das ist und in welches Objekt er casten muss
	
	private UUID guess;

	public Answer(UUID guess)
	{
		super();
		this.guess = guess;
	}

	public UUID getGuess()
	{
		return guess;
	}
	
	
	
	
}
