package at.campus02.pr3.quizduell.messages;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Question implements Serializable
{

	private static final long serialVersionUID = 1L; 	
	
	private int level;
	private String text;
	private Map<UUID, String> answers = new HashMap<>();
	private boolean last = false;
	
	
	public Question(int level, String text)
	{
		super();
		this.level = level;
		this.text = text;
	}

	public int getLevel()
	{
		return level;
	}

	public String getText()
	{
		return text;
	}

	public Map<UUID, String> getAnswers()
	{
		return answers;
	}
	
	public void setLast() {
		last = true;
	}

	public boolean isLast()
	{
		return last;
	}
	
	
	
	
	
	
	
	
	
	
}
