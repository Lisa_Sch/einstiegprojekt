package at.campus02.pr3.quizduell.messages;

import java.io.Serializable;

public class Player implements Serializable
{
	private static final long serialVersionUID = 2L;
	
	private String name;

	public Player(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
	
	

}
