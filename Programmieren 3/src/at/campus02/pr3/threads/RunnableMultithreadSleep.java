package at.campus02.pr3.threads;

public class RunnableMultithreadSleep
{

	public static void main(String[] args)
	{
		TimerRunnable r1 = new TimerRunnable();			// damit ich auf Methoden von TimerRunnable zugreifen kann
		TimerRunnable r2 = new TimerRunnable();
		TimerRunnable r3 = new TimerRunnable();
		TimerRunnable r4 = new TimerRunnable();
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		Thread t3 = new Thread(r3);
		Thread t4 = new Thread(r4);
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		try
		{
			Thread.sleep(10000);
			
			r1.stopThread();
			r2.stopThread();
			r3.stopThread();
			r4.stopThread();
		
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		 
		
		

	}

}
