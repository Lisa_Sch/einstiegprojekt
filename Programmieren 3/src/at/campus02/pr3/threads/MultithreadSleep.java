package at.campus02.pr3.threads;

public class MultithreadSleep
{

	public static void main(String[] args)
	{
		Thread t1 = new TimerThread();
		Thread t2 = new TimerThread();
		Thread t3 = new TimerThread();
		Thread t4 = new TimerThread();

		t1.start();		// parallele Programme, Thread kann ich nur einmal starten. Ich muss neues Objekt erstellen und das dann starten.
		t2.start();
		t3.start();
		t4.start();
		
		try
		{
			Thread.sleep(10000); 			// legt Hauptthread(main) f�r 10 Sekunden schlafen 
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		
	}

}
