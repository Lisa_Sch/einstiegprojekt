package at.campus02.pr3.threads;

public class Friend implements Runnable
{
	
	private Friend friend;								// Beziehung: Der Freund, vor dem verbeugt wird
	private String name;
	
	
	

	public Friend(String name)
	{
		super();
		this.name = name;
	}



	public Friend getFriend()
	{
		return friend;
	}



	public void setFriend(Friend friend)				// ein Freund merkt sich den anderen Freund
	{
		this.friend = friend;
	}



	
	public String getName()
	{
		return name;
	}



	public void run()
	{
		bow();
	}

	public synchronized void bow () 		// wenn in Methode drin, ist Methode f�r anderen Thread gesperrt
	{
		System.out.println(name + " verbeugt sich vor" + this.getFriend().getName());
		this.getFriend().bowBack(); 		// getFriend mit anderem Friend aufrufen -> Freund gesperrt weil Methode synchronized -> Wenn beide gleichzeitig in Methode sind beide gesperrt
	}
	
	public synchronized void bowBack()      // wenn in Methode drin, ist Methode f�r anderen Thread gesperrt
	{
		System.out.println(name+ " erwidert die Verbeugung von " + this.getFriend().getName());
	}
}
