package at.campus02.pr3.threads;

public class DeadlockDemo
{

	public static void main(String[] args)
	{
		Friend f1 = new Friend("Max");		
		Friend f2 = new Friend("Maria");
		
		f1.setFriend(f2);
		f2.setFriend(f1);
		
		Thread t1 = new Thread(f1);				// 2 Threads parallel
		Thread t2 = new Thread(f2);
		
		t1.start();
		t2.start();

	}

}
