package at.campus02.pr3.threads;

public class TimerThread extends Thread
{

	@Override
	public void run()
	{
		while(true) {
			System.out.println("Thread: " + Thread.currentThread().getId());
			try
			{
				Thread.sleep(200);				// Ausführung des Threads künstlich schlafen legen 
			
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			} 						
		}
	}
	
}
