package at.campus02.pr3.threads;

public class TimerRunnable implements Runnable
{
	
	private boolean isRunning = true;
	
	public void stopThread() {
		this.isRunning=false;
	}

	@Override
	public void run()
	{
		
		while(isRunning) {
			System.out.println("Thread: "+ Thread.currentThread().getId());
			try
			{
				Thread.sleep(200);
			} catch (InterruptedException e)
			{
			
				e.printStackTrace();
			}
		}

	}

}
